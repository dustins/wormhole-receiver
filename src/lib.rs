mod chunk_organizer;
mod reader;

use std::error::Error;
use std::sync::Arc;
use std::thread;

use crossbeam::channel;
use crossbeam::channel::select;

pub use crate::config::*;
use crate::chunk_organizer::{ChunkOrganizer, PartialChunk};

mod config;

pub fn run(config: Arc<Config>) -> Result<(), Box<dyn Error>> {
    // setup signaling channels
    let ctrl_c_events = ctrl_channel().expect("Unable to get Ctrl-c channel");

    // setup data flow channels
    let (reader_input, reader_output) = channel::unbounded();
    let (decoder_input, decoder_output) = channel::unbounded();

    spawn_reader(config.clone(), reader_input);
    spawn_organizer(config.clone(), reader_output, decoder_input);

    let mut total_bytes_read = 0;
    loop {
        select! {
            recv(ctrl_c_events) -> _ => {
                if config.debug {
                    println!("Received signal of Ctrl-c");
                }

                break;
            }
            // recv(reader_output) -> result => {
            //     match result {
            //         Ok(buffer) => {
            //             total_bytes_read += buffer.len() - 6;
            //         },
            //         Err(e) => {
            //             eprintln!("Done listening to reader. {}", e);
            //             break;
            //         }
            //     }
            // }
        }
    }

    if config.debug {
        println!("Read {} bytes", total_bytes_read);
    }

    Ok(())
}

fn spawn_reader(config: Arc<Config>, reader_input: channel::Sender<Vec<u8>>) {
    if config.debug {
        println!("Spawning [Reader]");
    }

    // todo decide how to handle multiple reader threads
    thread::Builder::new()
        .name("Reader".to_string())
        .spawn(move || {
            reader::read(config.clone(), reader_input);

            if config.debug {
                println!("[{}] Shutting down", thread::current().name().unwrap());
            }
        }).expect("Failed to spawn thread.");
}

fn spawn_organizer(config: Arc<Config>, reader_output: channel::Receiver<Vec<u8>>, decoder_input: channel::Sender<PartialChunk>) {
    if config.debug {
        println!("Spawning [Organizer]");
    }

    thread::Builder::new()
        .name("Organizer".to_string())
        .spawn(move || {
            let mut organizer = ChunkOrganizer::new(config);

            let chunks = organizer.completed_channel();
            loop {
                select! {
                    recv(chunks) -> result => {
                        match result {
                            Ok(partial_chunk) => {
                                println!("found chunk {}", partial_chunk.chunk_id);
                                decoder_input.send(partial_chunk).unwrap();
                            },
                            _ => {
                                println!("done with chunks");
                                break;
                            }
                        }
                    },
                    recv(reader_output) -> result => {
                        match result {
                            Ok(slice) => {
                                organizer.add_slice(slice);
                                // println!("added slice");
                            },
                            _ => {
                                println!("done with slices");
                                break;
                            }
                        }
                    }
                 }
            }

            println!("Shutting down organizer");
        }).expect("Failed to spawn thread.");
}

/// Returns a channel that fires when Ctrl-C is pressed
fn ctrl_channel() -> Result<crossbeam::Receiver<()>, ctrlc::Error> {
    let (sender, receiver) = crossbeam::bounded(10);
    ctrlc::set_handler(move || {
        println!("Ctrl-c pressed. Exiting.");
        let _ = sender.send(());
    })?;

    Ok(receiver)
}