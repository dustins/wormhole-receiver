use std::error::Error;
use std::fs::File;
use std::io::{Read, Cursor};
use std::net::{SocketAddr, TcpListener, TcpStream, UdpSocket};
use std::path::PathBuf;
use std::sync::Arc;
use std::thread;
use std::time::{Instant};
use byteorder::{BigEndian, LittleEndian, ReadBytesExt};

use crossbeam::channel;

use crate::{Config, Target};

pub fn read(config: Arc<Config>, reader_input: channel::Sender<Vec<u8>>) {
    match &config.source {
        Target::TCP(address) => {
            read_tcp_socket(config.clone(), address, &reader_input).unwrap();

            if config.debug {
                println!("Finished reading TCP socket.");
            }
        },
        Target::UDP(address) => {
            read_udp_socket(config.clone(), address, &reader_input).unwrap();

            if config.debug {
                println!("Finished reading UDP socket.");
            }
        },
        Target::File(path) => {
            read_file(config.clone(), path, &reader_input).unwrap();
        }
        Target::Bench => {
            panic!("wormhole-receiver not able to listen to bench")
        }
    }
}

fn read_read(config: Arc<Config>, read: &mut dyn Read, reader_input: &channel::Sender<Vec<u8>>) -> Result<(), Box<dyn Error>> {
    let start = Instant::now();
    let mut total_bytes_read = 0;
    let capacity = config.packet_size + 6;
    loop {
        let mut buffer = Vec::with_capacity(capacity);
        buffer.resize(capacity, 0u8);
        match read.read(&mut buffer[0..capacity]) {
            Ok(0) => break,
            Ok(bytes_read) => {
                total_bytes_read += bytes_read - 8;
                // println!("read {} bytes", bytes_read);
                buffer.resize(bytes_read, 0u8);
                reader_input.send(buffer)?
            }
            Err(_) => {
                panic!("Failed to read from Reader.");
                // return Err(Box::new(e));
            }
        }
    }

    if config.debug {
        println!("[{}] Read {} bytes in {:?}", thread::current().name().unwrap(), total_bytes_read, start.elapsed());
    }

    Ok(())
}

fn read_file(config: Arc<Config>, path: &PathBuf, reader_input: &channel::Sender<Vec<u8>>) -> Result<(), Box<dyn Error>> {
    if !path.exists() {
        return Err(Box::<dyn Error>::from(format!("File `{:?}` does not exist.", path)));
    }

    if !path.is_file() {
        return Err(Box::<dyn Error>::from(format!("Path `{:?}` doesn't point to a file.", path)));
    }

    let mut file_handle = File::open(path)?;
    read_read(config, &mut file_handle, &reader_input)
}

fn read_udp_socket(config: Arc<Config>, address: &SocketAddr, reader_input: &channel::Sender<Vec<u8>>) -> Result<(), Box<dyn Error>> {
    if config.debug {
        println!("Binding to `{}` to listen for connections.", address);
    }

    let mut total_bytes_read = 0;
    let socket = UdpSocket::bind(address)?;
    loop {
        let mut buffer = Vec::with_capacity(config.packet_size);
        buffer.resize(config.packet_size, 0u8);
        match socket.recv(&mut buffer) {
            Ok(bytes_read) => {
                total_bytes_read += bytes_read;
                // println!("read {} bytes", bytes_read);
                reader_input.send(buffer);
            },
            Err(e) => panic!("Failed to read from Reader.")
        }
    }

    if config.debug {
        println!("[{}] Done listening to `{}` for connections.", thread::current().name().unwrap(), address);
    }

    Ok(())
}

fn read_tcp_socket(config: Arc<Config>, address: &SocketAddr, reader_input: &channel::Sender<Vec<u8>>) -> Result<(), Box<dyn Error>> {
    if config.debug {
        println!("Binding to `{}` to listen for connections.", address);
    }

    let listener = TcpListener::bind(address)?;
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                if config.debug {
                    println!("[{}] Received new connection. {:?}", thread::current().name().unwrap(), stream);
                }

                handle_client(config.clone(), &stream, reader_input);

                if config.debug {
                    println!("[{}] Closing connection with {:?}", thread::current().name().unwrap(), stream);
                }
            }
            Err(e) => return Err(Box::new(e))
        }
    }

    if config.debug {
        println!("[{}] Done listening to `{}` for connections.", thread::current().name().unwrap(), address);
    }

    Ok(())
}

fn handle_client(config: Arc<Config>, mut stream: &TcpStream, reader_input: &channel::Sender<Vec<u8>>) {
    read_read(config, &mut stream, reader_input).unwrap();
}