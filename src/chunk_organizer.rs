use crate::Config;
use std::sync::Arc;
use std::collections::VecDeque;

use crossbeam::channel;
use std::io::{Cursor, BufRead, Read};
use byteorder::{ReadBytesExt, LittleEndian};

pub struct ChunkOrganizer {
    config: Arc<Config>,

    chunks: VecDeque<PartialChunk>,
    last_completed_chunk: usize,

    completed_tx: channel::Sender<PartialChunk>,
    completed_rx: channel::Receiver<PartialChunk>,
}

impl ChunkOrganizer {
    pub fn new(config: Arc<Config>) -> ChunkOrganizer {
        let (completed_tx, completed_rx) = channel::bounded(10);
        ChunkOrganizer {
            config,

            chunks: VecDeque::with_capacity(96),
            last_completed_chunk: 0,

            completed_tx,
            completed_rx
        }
    }

    pub fn add_slice(&mut self, mut slice: Vec<u8>) {
        let mut cursor = Cursor::new(&slice);
        let length = cursor.read_u16::<LittleEndian>().unwrap();
        let packet_number = cursor.read_u32::<LittleEndian>().unwrap() as usize;
        slice.rotate_left(6);
        slice.resize(length as usize, 0u8);

        let chunk_id = packet_number / self.config.total_shard_count() + 1;
        let slice_id = packet_number % self.config.total_shard_count() + 1;
        let offset = chunk_id - self.last_completed_chunk;
        if offset > 0 {
            match self.chunks.get_mut(offset - 1) {
                Some(chunk) => {
                    chunk.add_slice(slice_id as usize, slice);
                },
                None => {
                    let mut chunk = PartialChunk::new(chunk_id, self.config.total_shard_count(), self.config.shards);
                    chunk.add_slice(slice_id as usize, slice);
                    self.chunks.push_back(chunk);
                }
            }
        }

        loop {
            if let Some(chunk) = self.chunks.front() {
                if !chunk.complete {
                    break
                }
            } else {
                break
            }

            self.last_completed_chunk = chunk_id;
            self.completed_tx.send(self.chunks.pop_front().unwrap());
        }
    }

    pub fn completed_channel(&self) -> channel::Receiver<PartialChunk> {
        self.completed_rx.clone()
    }
}

pub struct PartialChunk {
    pub chunk_id: usize,
    slices: Vec<Option<Vec<u8>>>,
    min_slices: usize,
    current_slices: usize,
    complete: bool
}

impl PartialChunk {
    pub fn new(chunk_id: usize, max_slices: usize, min_slices: usize) -> PartialChunk {
        let mut slices = Vec::with_capacity(max_slices);
        slices.resize(max_slices, None);
        PartialChunk {
            chunk_id,
            slices,
            min_slices,
            current_slices: 0,
            complete: false
        }
    }

    pub fn add_slice(&mut self, slice_id: usize, slice: Vec<u8>) {
        self.slices.insert(slice_id, Some(slice));
        self.current_slices += 1;

        self.complete = self.current_slices >= self.min_slices;
    }
}