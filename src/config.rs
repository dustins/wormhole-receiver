use structopt::StructOpt;
use std::path::PathBuf;
use std::net::SocketAddr;
use std::error::Error;

pub const DEFAULT_CHANNEL_SIZE: usize = 32;
pub const READER_CHANNEL_SIZE: usize = DEFAULT_CHANNEL_SIZE;
pub const ENCODER_CHANNEL_SIZE: usize = DEFAULT_CHANNEL_SIZE;
pub const SENDER_CHANNEL_SIZE: usize = DEFAULT_CHANNEL_SIZE;

pub const BUFFER_SIZE: usize = 8192 * 12;

#[derive(StructOpt, Debug)]
pub struct Config {
    /// Activate debug mode
    #[structopt(short, long)]
    pub debug: bool,

    /// Verbose mode (-v, -vv, -vvv, etc.)
    #[structopt(short, long, parse(from_occurrences))]
    pub verbose: u8,

    /// How many threads to listen for incoming TCP connections
    #[structopt(long, default_value = "1")]
    pub reader_threads: usize,

    /// Which CPUs to run the reader(s) on
    #[structopt(long)]
    pub reader_cpuset: Option<Vec<u8>>,

    /// How many threads to use for encoding for incoming TCP connections
    #[structopt(long, default_value = "1")]
    pub encoder_threads: usize,

    /// Which CPUs to run the encoder(s) on
    #[structopt(long)]
    pub encoder_cpuset: Option<Vec<u8>>,

    /// How many threads to use for
    #[structopt(long, default_value = "1")]
    pub sender_threads: usize,

    /// Which CPUs to run the forwarder(s) on
    #[structopt(long)]
    pub sender_cpuset: Option<Vec<u8>>,

    /// Number of data shards
    #[structopt(short, long, default_value = "10")]
    pub shards: usize,

    /// Number of parity shards
    #[structopt(short, long, default_value = "2")]
    pub parity: usize,

    /// Size of packets to use
    #[structopt(long, default_value = "1024")]
    pub packet_size: usize,

    /// How many milliseconds each chunk should be spread over
    #[structopt(short, long)]
    pub burst_guard: Option<u8>,

    /// Source to read from (example: `file:someFile.txt`, `tcp:192.168.0.2:4488`, `udp:192.255.255.255:4488`)
    #[structopt(name = "SOURCE", parse(try_from_str = parse_target))]
    pub source: Target,

    /// Target to send to (example: `file:someFile.txt`, `tcp:192.168.0.4:4488`, `udp:192.255.255.255:4488`)
    #[structopt(name = "TARGET", parse(try_from_str = parse_target))]
    pub target: Target
}

impl Config {
    pub fn total_shard_count(&self) -> usize {
        self.shards + self.parity
    }
}

#[derive(Debug)]
pub enum Target {
    File(PathBuf),
    TCP(SocketAddr),
    UDP(SocketAddr),
    Bench
}

fn parse_target(target: &str) -> Result<Target, Box<dyn Error>> {
    if target.eq("bench") {
        return Ok(Target::Bench);
    }

    let delimiter = match target.find(":") {
        Some(index) => {
            index
        },
        None => return Err(Box::<dyn Error>::from("bad format"))
    };

    let (protocol, path) = target.split_at(delimiter);
    match protocol {
        "tcp" => {
            Ok(Target::TCP(path[1..].parse::<SocketAddr>().unwrap()))
        },
        "udp" => {
            Ok(Target::UDP(path[1..].parse::<SocketAddr>().unwrap()))
        }
        "file" => {
            Ok(Target::File(path[1..].parse::<PathBuf>().unwrap()))
        },
        _ => Err(Box::<dyn Error>::from(format!("Unable to parse `{}`", target)))
    }
}